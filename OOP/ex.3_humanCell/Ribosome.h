#pragma once
#include "Protein.h"
#include "AminoAcid.h"

/* a very short class that represents the Ribosome (it creates proteins)*/
class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;
};