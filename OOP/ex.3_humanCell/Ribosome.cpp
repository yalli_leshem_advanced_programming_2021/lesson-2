#include "Ribosome.h"

/*
* function gets an RNA transcript and returns its protein
* will return nullptr if the 'get_amino_acid' function returns 'UNKNOWN'

*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	// declare variables
	const int CODON_LEN = 3;
	int i = 0;
	AminoAcid aminoAcid;
	std::string codon;

	// init protein object
	Protein* protein = new Protein;
	protein->init();

	// takes each time 3 nucleotides from the RNA transcript
	while (CODON_LEN <= RNA_transcript.length() - i)
	{
		codon = RNA_transcript.substr(i, CODON_LEN); // get codon
		aminoAcid = get_amino_acid(codon); // get amino acid

		// check if amino acid is valid
		if (AminoAcid::UNKNOWN == aminoAcid)
		{
			protein->clear();
			return nullptr;
		}
		else // if valid
		{
			protein->add(aminoAcid); // add amino acid to the protein list
			i += CODON_LEN; // go to the next 3 Nucleotides
		}
	}
	
	return protein;
}