#include "Mitochondrion.h"

/*
*	function initilizes a new Mitochodrion object with 0 and false
*/
void Mitochondrion::init()
{
	this->_glucose_level = 0;
	this->_has_glucose_receptor = false;
}

/*
*	function checks if the protein has a glucose receptor
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	const int RECEPTOR_LEN = 7;
	const int GLUCOSE_RECEPTOR_FORMATION[RECEPTOR_LEN] = { AminoAcid::ALANINE, AminoAcid::LEUCINE, AminoAcid::GLYCINE, AminoAcid::HISTIDINE, AminoAcid::LEUCINE, AminoAcid::PHENYLALANINE, AminoAcid::AMINO_CHAIN_END };
	
	AminoAcidNode* first = new AminoAcidNode;
	first = protein.get_first();

	this->_has_glucose_receptor = true; // reset the receptor flag
	for (int i = 0; i < RECEPTOR_LEN; i++)
	{
		if (GLUCOSE_RECEPTOR_FORMATION[i] == first->get_data()) // if equel, go to the next amino acid
			first = first->get_next();
		else
			this->_has_glucose_receptor = false; 
	}

}

// sets the glucose level
void Mitochondrion::set_glucose(const unsigned int glucose_units) { this->_glucose_level = glucose_units; }

// returns true if the mito can produce atp
bool Mitochondrion::produceATP() const
{
	const int MIN_GLUCOSE_LEVEL = 50;

	// for it to produce atp, the mito has to have a glucose receptor and the glucose level has to be at least 50
	return (this->_has_glucose_receptor && MIN_GLUCOSE_LEVEL <= this->_glucose_level);
}
