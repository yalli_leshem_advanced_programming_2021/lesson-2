#pragma once
#include <string>
#include <iostream>
#include "Protein.h"
#include "AminoAcid.h"
#include "Ribosome.h"
#include "Mitochondrion.h"
#include "Nucleus.h"

/* 
* class represents the human cell. 
* it holds some of the elements that are in a human cell
* it simulates a few key functions that are happening inside the human cell
*/
class Cell
{
public:
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();

private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glucose_receptor_gene;
	unsigned int _atp_units;
};
