#include "Cell.h"

/*
*	function initializes a new cell object
*	initializes the nucleus object
*	initializes the mitochondrion object
*	sets the glucose receptor gene
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence); // initialize the nucleus object
	this->_mitochondrion.init(); // initialize the mitochondrion object

	this->_glucose_receptor_gene = glucose_receptor_gene; // sets the glucose receptor gene
}

/*
* function gets atp
* returns true if produces atp succesfuly, if not, returns false
* if protein couldnt be made, throws an error and exits the program
*/
bool Cell::get_ATP()
{
	const int MIN_GLUCOSE_LEVEL = 50;

	std::string RNA_transcript;
	Protein* protein = new Protein;
	bool produced_atp;

	// gets the RNA transcript of the gene
	RNA_transcript = this->_nucleus.get_RNA_transcript(this->_glucose_receptor_gene);

	// gets the protein of the transcript
	protein = this->_ribosome.create_protein(RNA_transcript);

	if (protein == nullptr) // if there was an error during the making of the protein, throw an error and exit the code
	{
		std::cerr << "An error accured during the protein creation" << std::endl;
		_exit(0);
	}
	else
	{
		this->_mitochondrion.insert_glucose_receptor(*protein); // check if the protein can really  produce glucose 
		this->_mitochondrion.set_glucose(MIN_GLUCOSE_LEVEL); // sets the glucose level to the minimal amount required to produce atp

		produced_atp = this->_mitochondrion.produceATP(); // can you produce atp?

		if (produced_atp) // if you can, changes the atp units to 100
			this->_atp_units = 100;

		return produced_atp;
	}
}