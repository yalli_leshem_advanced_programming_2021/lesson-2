#pragma once
#include "Protein.h"
#include <string>
#include <iostream>
#include "AminoAcid.h"

/* a short class that represents the mitochondrion*/
class Mitochondrion
{
public:
	void init();

	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glucose_units);
	bool produceATP() const;

private:
	unsigned int _glucose_level;
	bool _has_glucose_receptor;
};