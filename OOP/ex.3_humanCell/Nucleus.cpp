#include "Nucleus.h"

/* implement all the functions of the Gene class*/
/*
	function initializes a new Gene object with the given data
	input: start/end - unsigned int that represents the begining and the end of the gene
		   on_complementary_dna_strand - bool that is true if it is
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	if (end < start)
	{
		std::cerr << "An error accured during the initialization of the gene: \nend field has to be bigger then start" << std::endl;
		_exit(1);
	}
	else
	{
		this->_start = start;
		this->_end = end;
		this->_on_complementary_dna_strand = on_complementary_dna_strand;
	}
}

// gene getters
unsigned int Gene::get_start() const { return this->_start; }
unsigned int Gene::get_end() const { return this->_end; }
bool Gene::is_on_complementary_dna_strand() const { return this->_on_complementary_dna_strand; }

// gene setters
void Gene::set_start_and_end(const unsigned int start, const unsigned int end)
{
	if (end < start)
		std::cerr << "An error accured when tried to set the start and the end of the gene: \nDID NOT CHANGE \nend field has to be bigger then start" << std::endl;
	else
	{
		this->_start = start;
		this->_end = end;
	}
}

void Gene::set_if_on_complementary_dna_strand(const bool on_complementary_dna_strand) { this->_on_complementary_dna_strand = on_complementary_dna_strand; }


/* implement all the functions of the Nucleus class*/

/*
*	function initializes a new Nucleus object 
*/
void Nucleus::init(const std::string dna_sequence)
{
	const int NUM_OF_VARIATIONS = 4;
	const char COMPLEMENTARYS[NUM_OF_VARIATIONS][2] = { {'G', 'C'}, {'T', 'A'}, {'C', 'G'}, {'A', 'T'} };
	
	bool valid_sequence = false;

	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = dna_sequence;

	for (unsigned int i = 0; i < dna_sequence.length(); i++) // go through all the characters in the DNA sequesnce 
	{
		valid_sequence = false; // reset flag

		for (int j = 0; j < NUM_OF_VARIATIONS; j++) // check if the current letter of the sequence is one of the possible variations
		{
			if (dna_sequence[i] == COMPLEMENTARYS[j][0]) // if its equel to one of the variations
			{
				this->_complementary_DNA_strand[i] = COMPLEMENTARYS[j][1]; // make the complementary letter accordingly
				valid_sequence = true; // raise flag
			}
		}
		if (!valid_sequence) // if after going through all the variations the flag still isnt up, raise exeption and exit code
		{
			std::cerr << "An error accured during the initialization of the nucleus: \nDNA sequence can only contain the letters: A, G, C, T" << std::endl;
			_exit(1);
		}
	}
}

/*
*	function makes the RNA transcript of a gene in the DNA starnd
*	input: the gene object that has all the information of the RNA
*	output: an RNA transcript where ant 'T' Nucleotide becomes 'U'
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA_transcript = "";
	std::string from_DNA_strand;

	// get the wanted DNA strand
	if (gene.is_on_complementary_dna_strand()) // if true then the gene is from the complementary dna strand
		from_DNA_strand = this->_complementary_DNA_strand;
	else
		from_DNA_strand = this->_DNA_strand;

	// make sure the gene has valid input
	if (gene.get_end() >= from_DNA_strand.length()) 
	{
		std::cerr << "the end of the gene can't be bigger then the length of the dna strand" << std::endl;
		return "";
	}
	else
	{
		for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++) // go through all the Nucleotides
		{
			// because RNA transcript doesnt have 'T' Nucleotide, if the Nucleotide is 'T' then change it to 'U'
			if (from_DNA_strand[i] == 'T')
				RNA_transcript += 'U';
			else
				RNA_transcript += from_DNA_strand[i];
		}

		return RNA_transcript;
	}
}

/*
* function returns the reverse DNA strand
* output: the reversed DNA strand
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversed_DNA_strand = this->_DNA_strand; // make an equivalent DNA strand 

	std::reverse(reversed_DNA_strand.begin(), reversed_DNA_strand.end()); // use reverse function (it reverses the chars in a string)
	return reversed_DNA_strand;
}

/*
*	function returns the bumber of times the codon is in the first DNA strand
*	input: the codon we want to check
*	output: if the codons length is 3 return the number of times the codon is in the DNA strand
         if the codons length is NOT 3 return 0
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	const int CODON_LENGTH = 3;
	std::size_t found = 0;
	unsigned int num_of_codon_appearances = 0;

	// make sure the codon is valid
	if (codon.length() != CODON_LENGTH)
	{
		std::cerr << "An error accured when trying to get the codon count: \nA codon has to be 3 letters" << std::endl;
		return 0;
	}
	else
	{
		do
		{
			found = _DNA_strand.find(codon, found+CODON_LENGTH);
			num_of_codon_appearances++;

		} while (found != std::string::npos);

		return num_of_codon_appearances - 1; //return result is -1 because the counter goes up by one more after there are no more codons found
	}
}